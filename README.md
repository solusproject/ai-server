This repository was planned to host an AI server, however due to new changes in how Quinn's AI operates, it will no longer need an AI server.

We will instead be hosting a pre-trained model on the /ai repository.
